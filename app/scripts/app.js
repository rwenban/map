'use strict';

var app =  angular.module('mapApp', [ 'ui.bootstrap', 'ui.map', 'angularLocalStorage' ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/launchmodal', {
        templateUrl: 'views/launchmodal.html',
        controller: 'LaunchmodalCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/map', {
        templateUrl: 'views/map.html',
        controller: 'MapCtrl'
      })
      .when('/info', {
        templateUrl: 'views/info.html',
        controller: 'InfoCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

app.run(function ($rootScope, $location, storage) {
//    $rootScope.activeTab = 'usage';
//    $location.path("usage");

    console.log ( "run" );

    // New look for Google Maps
    google.maps.visualRefresh = true;

    google.maps.event.trigger('.map', 'resize');
});


// The map height should be the height of the window minus the height of the footer ( 40px ) class- footer
// And minus the y position of the 'content' window class - content
app.directive('resize', function ($window) {

    return function (scope, element) {

        // calculate DOM values
        // footer
        var f = angular.element( '#footer');

        if( f ) var footerHeight = f.height();

        // the content container which the map is within
        var content = angular.element( '#page-content');

       // var contentHeight = content.height();
        var row = angular.element( '#row' );

        var top = row.position().top;

        var footer = angular.element( '#footer' );

        var sideBarHeight = angular.element( '#side-bar').height();

        var w = angular.element($window);

        scope.getWindowDimensions = function () {

            return { 'h': w.height(), 'w': w.width() };

        };
        scope.$watch(scope.getWindowDimensions, function ( newValue, oldValue ) {

            // resize the map to optimize height

            // simply adjust the available space with the container's y position

            console.log(" Properties: ")
            console.log(" Properties | top: " + top)
            console.log("height - newValue: " + newValue.h ); //  the height of the visible window
            console.log("footerHeight: "+ footer.height() );
            console.log("sideBarHeight: "+ sideBarHeight );

            var sum = newValue.h - top - footer.height();

            $('.map').height( Math.max( sum, sideBarHeight )  );

        }, true);

        w.bind('resize', function () {

            scope.$apply();
        });
    }
});



