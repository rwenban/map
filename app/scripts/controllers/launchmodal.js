'use strict';

// see: http://angular-ui.github.io/bootstrap/

app.controller('LaunchmodalCtrl' , function ( $scope, $modal, $log ) {


    })

    // TODO - look into deep copy: http://docs.angularjs.org/api/angular.copy
    // evoked when the model is launched


    // TODO - we could pass the location reference the required properties from that
    /**
     *
     * @param $scope
     * @param $modalInstance
     * @param values - both the location and the marker are passed to the modal
     * @constructor
     */
    var ModalInstanceCtrl = function ($scope, $modalInstance, values ) {

        console.log( "\nModal");

        // will pass copy so the 'result' is responsible for displaying the changed values
        $scope.input = {};

        // the locations unique id
        $scope.input.id = values.location.id;

        // the locations title
        $scope.input.title = values.location.title;

        // the locations description
        $scope.input.description = values.location.description;

        // the url property - TODO
        $scope.input.url = values.location.url;

        // property which is set to true if the user is editing an existing marker
        $scope.input.edit = values.edit;

        // the property to set if the user wants to delete this marker
        $scope.input.delete = false;

        // the full set of available icon
        $scope.input.icons = values.icons;

        // the markers current icon
        $scope.input.icon = values.icon;



        /**
         * Evoked when the user closes the modal
         * Passes the data back to the callee
         */
        $scope.ok = function () {

            $modalInstance.close( $scope.input );
        };

        /**
         * Evoked when the user selects the 'delete' button
         * Passes the data back to the callee, with the instruction to delete the marker
         */
        $scope.delete = function () {

            $scope.input.delete = true;

            $modalInstance.close( $scope.input );
        };

        /**
         * Evoked when the user selects the 'cancel' button
         */
        $scope.cancel = function () {

            $modalInstance.dismiss('cancel');
        };

        /**
         * Evoked when the user selects an icon
         * @param icon - the newly selected icon
         */
        $scope.selectIcon = function ( icon ) {

            $scope.input.icon = icon.icon;
        }
    };