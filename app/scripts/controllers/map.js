'use strict';

app.controller('MapCtrl', function ( $scope,  $modal, $log, storage) {

    $scope.ready = false;

    $scope.geolocationAvailable = navigator.geolocation ? true : false;

    $scope.model = { myMap: undefined };

    //storage.bind($scope,'model');

    $scope.model.zoom = 15;

    $scope.model.markers = {};

    $scope.model.edit = false;

    $scope.model.center = {};

    $scope.model.id = 0;

    $scope.model.center = {
        latitude: 53,
        longitude: 0.01
    };

    $scope.model.icons = {};

    /**
     * Modal properties
     */
    // TODO - tidy defaults
    $scope.model.modalObj = {};

    $scope.model.modalObj.title = "Enter Title";

    $scope.model.modalObj.description = "";

    $scope.model.modalObj.url = "";

    $scope.model.modalObj.id = -1;

    $scope.model.modalObj.icon ="images/information.png";

    $scope.model.modalObj.icons = {};

    /*
     TODO - move the data to a JSON file
     */

    $scope.initialLocations = [
        ['Tate Modern', 51.507794,-0.099095, 7],
        ['Museum of London', 51.518063,-0.096695, 8],
        ['Giddy Up - Coffee', 51.521562,-0.093375, 9],
        ['The Barbican', 51.52016,-0.092838, 10],
        ['My House', 51.53, -0.0907, 6],
        ['The Acropolis', 37.971487,23.725757, 4],
        ['Coogee Beach', -33.923036, 151.259052, 5],
        ['Cronulla Beach', -34.028249, 151.157507, 3],
        ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
        ['Maroubra Beach', -33.950198, 151.259302, 1]
    ];

    $scope.model.locations = {};

    // a 'location' consists of:
    // id
    // marker
    // title
    // description
    // url

    //$storage.bind($scope, 'test', 'Some Default Text');

    $log.info("storage: " + storage );

    storage.bind($scope, 'test', 'Some Default Text');

    $scope.data = {};

    $scope.model.test = "A new default";

    storage.bind($scope, 'storedData', $scope.model );

    /**
     * TODO - extract from external data
     * initial icons
     */

        /*
            Path issue:

            full path
         /Users/russellwenban/localhosts/www.factornine.co.uk/development/map/app/images/apartment-3.png

         */
    // for 8888
    var path = "";

    path = ""

    var icons = {
        bus: {
            name: "bus",
            icon: "images/bus.png"
        },
        cinema: {
            name: "cinema",
            icon: "images/cinema.png"
        },
        coffee: {
            name: "coffee",
            icon: "images/coffee.png"
        },
        cycling: {
            name: "cycling",
            icon: "images/cycling.png"
        },
        info: {
            name: "information",
            icon: "images/information.png"
        },
        library: {
            name: "library",
            icon: "images/library.png"
        },
        map: {
            name: "map",
            icon: "images/map.png"
        },
        movie_rental: {
            name: "movie rental",
            icon: "images/movierental.png"
        },
        museum_art: {
            name: "art museum",
            icon: "images/museum_art.png"
        },
        museum_science: {
            name: "science museum",
            icon: "images/museum_science.png"
        },
        pizzaria: {
            name: "pizzaria",
            icon: "images/pizzaria.png"
        },
        playground: {
            name: "playground",
            icon: "images/playground.png"
        },
        restaurant: {
            name: "restaurant",
            icon: "images/restaurant.png"
        },
        star: {
            name: "star",
            icon: "images/star-3.png"
        },
        steamtrain: {
            name: "steam train",
            icon: "images/steamtrain.png"
        },
        supermarket: {
            name: "supermarket",
            icon: "images/supermarket.png"
        },
        tweet: {
            name: "tweet",
            icon: "images/tweet.png"
        },
        videogames: {
            name: "video games",
            icon: "images/videogames.png"
        },
        winebar: {
            name: "wine bar",
            icon: "images/winebar.png"
        },
        world: {
            name: "world",
            icon: "images/world.png"
        },
        zoo: {
            name: "zoo",
            icon: "images/zoo.png"
        },
        computers: {
            name: "computers",
            icon: "images/computers.png"
        },
        beach: {
            name: "beach",
            icon: "images/palm-tree-export.png"
        },
        forest: {
            name: "forest",
            icon: "images/forest.png"
        },
        clock: {
            name: "clock",
            icon: "images/clock.png"
        },
        apartment: {
            name: "apartment",
            icon: "images/apartment-3.png"
        },
        childmuseum: {
            name: "child museum",
            icon: "images/childmuseum01.png"
        },
        home: {
            name: "home",
            icon: "images/home-2.png"
        },
        toys: {
            name: "toys",
            icon: "images/toys.png"
        },
        recycle: {
            name: "recycle",
            icon: "images/recycle.png"
        }
    };

    $scope.model.icons = icons;

    $scope.model.modalObj.icons = icons;


    /**
     * Creates the initial text content to be used by the markers
     */
    $scope.createContent = function () {

        var string, title, location, marker;

        var initialLocation;

        // the default icon
        var  icon =  $scope.model.icons[ 'info' ].icon;

        var latLng;

        var id;

        for ( var i = 0; i < $scope.initialLocations.length; i++ ) {

             initialLocation = $scope.initialLocations[ i ];

             latLng = new google.maps.LatLng( initialLocation[1], initialLocation[2] );

             id = $scope.createId();

             marker = new google.maps.Marker({
                        map: $scope.model.myMap,
                        position: latLng,
                        icon: icon,
                        id:  id
                        });

            $scope.model.markers[ id ] =  marker ;

            location = { id: id, marker : marker,  title: initialLocation[ 0] };

            // console.log(i + " | createContent | id: " + id )

            $scope.model.locations[ id ] = location;
        }

       // reset the scrollable items
       $scope.reset();
    };

    /**
     * For the info window - this is the google information window
     */
    $scope.infowindow = new google.maps.InfoWindow({ });


    /**
     * The Google Maps option
     *
     * @type {{center: google.maps.LatLng, zoom: number, mapTypeId: *}}
     */
    $scope.mapOptions = {
        center: new google.maps.LatLng(  $scope.model.center.latitude,   $scope.model.center.longitude ),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };


    /**
     *  Initialization - adds all the markers from the model
     */
    $scope.init = function () {

        $scope.ready = true;

        $scope.createContent();
    };


    /**
     * A hack to resize the map
     *
     * @param $event
     * @param $params
     */
    $scope.onTilesLoaded = function ( $event, $params ) {

        if( !$scope.ready){

            $scope.init( );
            // necessary to force the map to be drawn correctly
            google.maps.event.trigger( $scope.model.myMap, 'resize');
        }
    };


    /**
     * Evoked when the user clicks on the map -
     * The Google Maps API has already added the 'marker' to the map?
     * Create a 'location' object and add to the 'locations' object
     *
     * @param $event
     * @param $params
     */
    $scope.addMarker = function( $event, $params ) {

        var len = $scope.count( $scope.model.locations );

        console.log('A | addMarker | markers len: ' +   len );

        // define the default marker
        var  icon =  icons[ 'info' ].icon;

        var id = $scope.createId();

        console.log("B | addMarker | new icon: " + icon );

        var  marker = new google.maps.Marker({
            map: $scope.model.myMap,
            position: $params[0].latLng,
            icon: icon,
            id:  id
        });


        $scope.model.markers[ id ] = marker;

        $scope.model.locations[ len ] = { id: id, marker : marker,  title: "default", description: "default" };

        $scope.reset();

        // automatically open the edit window
        $scope.open( marker );
    };


    /**
     * Evoked when the map zoom has changed
     * @param zoom - the Google Map zoom property
     */
    $scope.setZoomMessage = function ( zoom ) {

        $scope.model.zoom =  $scope.model.myMap.getZoom();
    };

    /**
     * Opens a panel with the window information
     *
     * @param marker
     */
    $scope.openMarkerInfo = function( marker) {

        console.log("openMarkerInfo: " + marker )

        $scope.currentMarker = marker;

        $scope.currentMarkerLat = marker.getPosition().lat();

        $scope.currentMarkerLng = marker.getPosition().lng();

        if( ! $scope.model.edit ) {

            $scope.model.modalObj.title = "Enter Title";

            $scope.model.modalObj.description = "Enter Description";
        }

        $log.info("openMarkerInfo | id: " + marker.id );

        $scope.model.modalObj.id = marker.id;

        $scope.model.modalObj.edit =  $scope.model.edit;

        if(! $scope.model.locations[ marker.id ] )
        {
            $log.info("openMarkerInfo | here ");
            // open pop-up!
            $scope.open( marker );

            return;
        }

        var string =  $scope.createInformation( $scope.model.locations[ marker.id ] );

        $scope.infowindow.setContent( string );

        $scope.infowindow.open( $scope.model.myMap, marker);
    };


    /**
     * Evoked when the user wants to edit a marker
     * @param item
     */
    $scope.edit = function ( item ) {

        $log.info( "Edit: " + item.id );

        $scope.model.edit = true;

        var location = $scope.model.locations[ item.id ];

        var marker =  $scope.model.markers[ item.id ];

        $log.info( "marker: " + marker );

        $log.info( "location: " + location );

        // need to set the details here!

        $scope.model.modalObj.id = item.id;

        $scope.model.modalObj.title = location.title;

        $scope.model.modalObj.description = location.description;

        $scope.model.modalObj.url = location.url;

        $scope.model.modalObj.edit =  $scope.model.edit;

        $scope.open( marker );

    };

    // Evoked when the map is moved
    $scope.onCenterChanged = function ( $center ){

        $scope.model.center.latitude  = $scope.model.myMap.getCenter().lat();

        $scope.model.center.longitude  = $scope.model.myMap.getCenter().lng();
    };


    /**
     * Finds the users position using geo-location
     */
    $scope.findMe = function () {

        if ($scope.geolocationAvailable) {

            console.log( "finding position" );

            navigator.geolocation.getCurrentPosition( function ( position ) {

                $scope.model.center = {
                    latitude: position.coords.latitude,

                    longitude: position.coords.longitude
                };

                $scope.model.myMap.setCenter( new google.maps.LatLng( $scope.model.center.latitude,  $scope.model.center.longitude ) );

                $scope.$apply();
            });
        }
    };


    // TODO - add guard
    /**
     * Evoked by the pan buttons
     * @param location
     */
    $scope.panTo = function ( location ) {

        console.log("panTo: " + location.title);

        console.log("location.latitude: " + location.latitude );

        console.log("location.longitude:  " + location.longitude );

        var position =  new google.maps.LatLng( location.latitude, location.longitude );

        $scope.model.myMap.panTo( position  );

    }


    /**
     * For the scrollable items
     *
     */

    //
    $scope.items = [];

    //
    $scope.canLoad = true;

    /**
     * Creates a simplified object containing only an 'id' 'title' and coordinates which is used by the repeat
     */
    $scope.addItems = function () {

        var item, marker, position;

        var latLng;

        for( var location in $scope.model.locations ){

            item = $scope.model.locations[ location];

            position = item.marker.position;

            latLng = ( item.marker.position );

            $scope.items.push( { id: item.id, title: item.title, latitude: latLng.lat(), longitude: latLng.lng() } );
        }
    };

    $scope.reset = function () {

        $scope.items = [];

        $scope.canLoad = true;

        $scope.addItems();
    };

    $scope.reset();




    // TODO - assignment like this will fail!
    // More like JSON? See: http://stackoverflow.com/questions/19118859/angular-creating-an-object-map
    // $scope.model.modalObj = { title: "Enter Description", description: "Enter Description" };

    // modal window

    //
    /**
     * Instantiate a new Modal window
     * @param marker
     */
    $scope.open = function ( marker ) {

        $log.info( "\nopen! | A | id: " +marker.id +"\n");

        $scope.model.modalObj.location = $scope.model.locations[ marker.id ];

        $log.info("open | icon: " + $scope.model.modalObj.location.marker.icon );

        $scope.model.modalObj.id = marker.id;

        $scope.model.modalObj.icon =  $scope.model.modalObj.location.marker.icon;

        var modalInstance = $modal.open({
            templateUrl: 'views/modal.html',
            controller: ModalInstanceCtrl,
            keyboard: true,
            resolve: {
                values: function () {
                    return $scope.model.modalObj;
                }
            }
        });

        /**
         * @value -  the object which is returned when the modal window closes
         */
        modalInstance.result.then( function ( value ) {

            console.log("\n\nresult | id: " + value.id );

            console.log("result | title: " + value.title );

            console.log("result | description: " + value.description );

          // first check if the user wants to delete the marker
            if( value.delete  )
            {
                console.log("result |  to delete | id: " + value.id);

                delete  $scope.model.locations[ value.id ];

                // necessary to remove the marker from the google map
                $scope.model.markers[ value.id ].setMap( null );

                delete $scope.model.markers[ value.id ];

                $scope.reset();

                return;
            }

            // set directly!

            // 1 icon
            $scope.model.locations[ value.id ].marker.setIcon( value.icon ) ;

            // 2 title
            $scope.model.locations[ value.id ].title = value.title;

            // 3 description
            $scope.model.locations[ value.id ].description = value.description;

            // 4 url
            $scope.model.locations[ value.id ].url = value.url;


            $scope.reset();

        }, function () {

            $log.info('Modal ( or cancelled ) dismissed at: ' + new Date() );

        });
    };

    /**
     * Searches within the aggregated items data
     * @param item - the input string
     * @returns an array of the matching items
     */
    $scope.search = function ( item ) {

        if( ! $scope.query )  {

            return $scope.items;

        }
        else {

            return ( item.title.indexOf( $scope.query ) != -1);
        }

    };

    /**
     * A utility function to count the number of objects contained in the supplied object
     * @param value
     * @returns number
     */
    $scope.count = function ( value ) {

        var count = 0;

        for ( var obj in value ){

            count++;
        }

        return count;
    };

    /**
     * A utility to displays the location properties
     * @param value
     */
    $scope.display = function ( value ) {

        var count = 0;

        var location;

        for ( var obj in value ){

            console.log( "Obj: " + obj + " | id: " + obj.marker );

            location = value [ obj ];

            console.log("Location: " + location.id + " | title: " + location.title );

            count++;
        }
    };

    /**
     * A utility which creates the HTML text for the map marker
     * @param location
     * @returns - string
     */
    $scope.createInformation = function ( location ) {

        var title, description;

        title = location.title || "";

        description = location.description || "";

        var string = '<div id="content" class="info-content">'+
                        '<h1>'+title+'</h1>'+
                        '<div id="bodyContent">'+
                        '<p>' + description +
                        '</div>'+
                    '</div>';

        return string;
    };

    /**
     *
     * @returns int
     */
    $scope.createId = function () {

        return $scope.model.id++;
    }


});
