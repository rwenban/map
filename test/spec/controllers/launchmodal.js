'use strict';

describe('Controller: LaunchmodalCtrl', function () {

  // load the controller's module
  beforeEach(module('mapApp'));

  var LaunchmodalCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LaunchmodalCtrl = $controller('LaunchmodalCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
